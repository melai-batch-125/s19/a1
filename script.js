
function Pokemon(name, lvl, hp){
	this.name = name;
	this.level = lvl;
	this.health = hp * 2;
	this.attack = lvl;
	this.tackle =  function(target){
		//console.log(target)	//contains wartortle obj

		console.log(`${this.name} tackled ${target.name}`);
		let remain = target.health - this.attack
		console.log(`${target.name}'s health is now reduced to ${remain}`)
		target.health = remain;

		if(remain < 10){
			this.faint();
		}
	};
	this.faint = function(){
		console.log(`${this.name} fainted`);
	}
}

//initialized an object with arguments
let voltorb = new Pokemon("voltorb", 19, 50);
let wartortle = new Pokemon("wartortle", 8, 40);
// let squirtle = new Pokemon("squirtle", 4, 80);

voltorb.tackle(wartortle);
voltorb.tackle(wartortle);
voltorb.tackle(wartortle);
voltorb.tackle(wartortle);

/*
Graded ACtivity: s19 a1

Create a new set of pokemon for pokemon battle.
Solve the health of the pokemon when tackle is invoked, current value of health should decrease.

if health is now below 10, invoke faint function

*/